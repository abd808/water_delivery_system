# Buliding a WebApp and an App in order to optimise the packaged drinking water supply


1. In this industry we have mainly 4 classes of users: manager, supervisor, delivery team, customers. A webApp is targetted for all the users except the delivery team. The application is targetted for the delivery team. First of all a company is registered with our app then the company can create some manger accounts.

1. The managers create the accounts of the supervisors 
1. The supervisors of a particular area will create the accounts of the customers and the delivery team under him/her.
1. With the WebApp the supervisor will be able to take orders and plan daily deliveries of the packaged drinking water cans. He will be able to track the delivery vehicles and their current stock. He can also view the inventory of all the stocks with him.
1. For each vehicle first a driver will scan the QR code designated to that particular vehicle and this combination of vehicle and the driver will be alloted a trip number unique to that day.
1. Furthermore the delivery boys will be able to join to any delivery vehicle of their choice by just scanning the QR code assignned to a particular vehicle and each delivery boy will also be assigned the trip number.
1. At the delivery point our delivery team will contact the customer and then scan a QR code particular to that customer. The customer then approves the details entered by the delivery boy. The delivery boy enters the count of filled cans delivered ,empty cans collected and damaged cans reported.
1. The delivery team would be able to apply for leaves through the application. 

## Note:
- Due to the Mongo Atlas cloud storage having time limit, and this product being built to serve as a prototype, it is currently not possible to view a live demo of the product.
- Attached is a demo video filmed immediately after completion of the coding phase.
    
    [Demo Video](https://drive.google.com/file/d/1d0R1q0Uclr0AMVpEJ9O0-vML0S6y-qyd/view?usp=sharing" target="_blank)